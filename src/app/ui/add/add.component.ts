import { Component, OnInit } from '@angular/core';
import { ServiceService  } from 'src/app/services/service.service';
import { NgModule } from '@angular/core';
import {Task} from 'src/app/models/task';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  taskDetails : any = {};
  parenttask :any = {};
  TaskId:number;
  Priority:number;
  TaskName:string;
  ParentTask:number;
  StartDate:Date;
  EndDate:Date;
  msg:any;
  Flag:number;
  constructor(private apiservice:ServiceService) { }

  ngOnInit() { 
    this.apiservice.getTaskData().subscribe(response=>{

      console.log("response" + JSON.stringify(response));
      //response.StartDate = response.StartDate.substr(0,10)
       this.parenttask = response;       
  })}
  reset()
  {
this.taskDetails = {};

  }
  addTask()
  { 
    // this.taskDetails = {
    //   "TaskID":this.TaskId,
    //   "Priority":this.Priority,
    //   "TaskName":this.TaskName,
    //   "ParentTask":this.ParentTask,
    //   "StartDate":this.StartDate,
    //   "EndDate" :this.EndDate,
    //   "Flag":0
    // }
    
      
    console.log(this.taskDetails);
    this.apiservice.postTaskData(this.taskDetails)
    //.subscribe(i=>this.msg = i);
    .subscribe(response=>{

      console.log(response);
      this.taskDetails = {};
      alert('Task Added!')
      
    });
      
      
  }
}
