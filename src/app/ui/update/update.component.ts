import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router, ActivatedRoute,NavigationExtras } from "@angular/router";
import { ServiceService  } from 'src/app/services/service.service';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import {Task} from 'src/app/models/task';
import { Observable } from 'rxjs/internal/Observable';
import { getLocaleDateTimeFormat } from '@angular/common/src/i18n/locale_data_api';
import { formatDate } from '@angular/common/src/i18n/format_date';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit{
  taskInfo : object;
  tasksinfo : any;
  parenttask :any = {};
  tasks :any;
  startDate:any;
  endDate:any;
  sDateF:any;
  eDateF:any;
  task:string;
  characters:any;
  constructor(private route: ActivatedRoute,private updateroute:Router,
    private service: ServiceService )
   { 
   }
    // console.log( 'Params'+ JSON.stringify(this.route.data));
  //  this.route.data.subscribe(params => {
  //    console.log('params' + params);
  //     this.taskInfo = params;
  //     //this.task = params["task"];
  //  })
  //  this.route.params.subscribe(params =>
  //    {
  //      this.task = params["taskId"];
  //      this.service.getTaskDetails(this.task).subscribe(data =>{
  //        this.taskInfo= data;
  //      });
  //    } )    

  ngOnInit() {
    console.log("Param: " + this.route.snapshot.params);
    this.task = this.route.snapshot.params.taskId;
    this.service.getTaskData().subscribe(response=>{

      console.log("response" + JSON.stringify(response));
      //response.StartDate = response.StartDate.substr(0,10)
       this.parenttask = response;       
  });
    this.service.getByTaskId(this.task).subscribe(response=>{

      console.log("response" + JSON.stringify(response));
    //this.tasksinfo = this.characters.find(p=> p.TaskId == this.task);
    this.tasksinfo = response ; 
    console.log("startdate"+  this.tasksinfo.StartDate);
    //this.sDateF = new Date(this.tasksinfo.StartDate).getFullYear() + "-"
    //+"0" + new Date(this.tasksinfo.StartDate).getMonth() + "-" + "0" +
    //new Date(this.tasksinfo.StartDate).getDate();
   
    this.sDateF = this.tasksinfo.StartDate.substr(0,10);
this.eDateF = this.tasksinfo.EndDate.substr(0,10);
    this.tasksinfo.StartDate = this.tasksinfo.StartDate.substr(0,10);
    this.tasksinfo.EndDate = this.tasksinfo.EndDate.substr(0,10);

   // this.sDateF = "2018-10-10";
    
    
    console.log('testtt' + this.sDateF);
    console.log('tt'+ JSON.stringify(this.taskInfo));   
   
       //this.startDate=this.tasksinfo.StartDate.getFullYear()+"-"+this.tasksinfo.StartDate.getMonth()+"-"+this.tasksinfo.StartDate.getDate();
       
       console.log("date"+this.startDate)
       }
      );
     // let test = this.characters.find(p=> p.TaskId == this.task);
    //this.tasksinfo = this.characters.find(p=> p.TaskId == this.task);
    
    console.log(this.tasksinfo);
  }
  // @NgModule({imports:[FormsModule]})
 CancelPage()
 {
  this.updateroute.navigate(['view']);
 }
 UpdatePage()
 {
   //this.tasksinfo.StartDate == new Date(this.sDateF);
   //this.tasksinfo.EndDate == new Date(this.eDateF);
   //console.log('edate'+ this.eDateF);
  console.log('update page' + this.tasksinfo.EndDate);
  this.service.putTaskData(this.tasksinfo)
  .subscribe(response=>{

    console.log("response" + response)});
    alert('Task updated');
 }
}
