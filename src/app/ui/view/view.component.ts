import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceService  } from 'src/app/services/service.service';
import { Router } from '@angular/router';
import {Task} from 'src/app/models/task';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  characters : any;
  columns : any;
  taskdata : {};
  taskDetails :any;
  tasksinfo:any;
  TaskId:number;
  Priority:number;
  TaskName:string;
  ParentTask:number;
  StartDate:Date;
  EndDate:Date;
  search:any={};
  
  list:string[] = ["Rahul"];
  constructor(private service: ServiceService,private router: Router
    ) { }

  ngOnInit()
  {
  this.characters= this.service.getCharacters();
   this.columns =  this.service.getColumns();
  //this.characters.StartDate = this.characters.StartDate.substr(0,10);
   //this.characters.EndDate = this.characters.EndDate.substr(0,10);
   
   //this.taskdata =
    this.service.getTaskData().subscribe(response=>{

    console.log("response" + JSON.stringify(response));
    //response.StartDate = response.StartDate.substr(0,10)
     this.characters = response;    
     //this.characters.StartDate = this.characters.StartDate.substr(0,10);
     console.log('kkk'+ this.characters.StartDate);
     }
    );
   //this.service.postTaskData();
  // console.log(this.columns);
   //console.log('view data');
   //console.log(this.taskdata);
  }

  redirectPage(char){
    
   this.router.navigate(['update/'+ char.TaskId] );
   //this.router.navigateByUrl('/update/');
    
  }
  deleteTask(char)
  { 
    console.log(char);
    this.service.deleteTaskData(char)
    //.subscribe(i=>this.msg = i);
    .subscribe(response=>{

      this.characters  = response; 
      
    });
      
      
  }
  endTask(char)
  {
    this.service.endTaskData(char)
    .subscribe(response=>{
      this.characters = response;
    }
    );

  }
  
  test()
  {
   //console.log( this.characters.any(i=>i.TaskId = 7));
    //this.characters = JSON.stringify(this.characters);
    //this.characters = this.characters.TaskId = 7;
    // this.characters =  this.characters.find(p=> p.TaskId == this.TaskId);
    console.log('search', this.search);
    let newCharacter = [];
    if(this.search != "" && Object.values(this.search).length > 0){
      this.characters.forEach(character => {
        Object.keys(this.search).forEach(search=>{
          if(character[search] == this.search[search])
          newCharacter.push(character);

        })
      
      })

      console.log("newCharacter1",newCharacter);
    this.characters = newCharacter;
    }

   // console.log(this.characters);
  }

}
