export class Task {
    TaskId:number;
    ParentTask:number;
    TaskName:string;
    Priority:number;
    StartDate:Date;
    StartDateF:Date;
    EndDateF:Date;
    EndDate:Date;
    Flag:number;

}
