

import { AppComponent } from './app.component';
import { ViewComponent } from './ui/view/view.component';
import { AddComponent } from './ui/add/add.component';
import { UpdateComponent } from './ui/update/update.component';
import {Routes, RouterModule} from "@angular/router";



export const applicationroutes:Routes= 
[
  {
    path:'update/:taskId',
    component : UpdateComponent,
    
  
  },
  
  {

    path:'add',
    component : AddComponent   

  },
  {
    path:'view',
    component : ViewComponent
  }
]
 
export class RoutingModule{}

 



