import { Injectable } from '@angular/core';
import { CHARACTERS } from 'src/app/mockdata';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import {Task} from 'src/app/models/task';
import { Observable } from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { stringify } from '@angular/core/src/util';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})


// const httpOptions = {
//   headers: new HttpHeaders({'Content-Type': 'application/json'})
// };ng serve

export class ServiceService
 {

  constructor(private http:HttpClient) { }


  getCharacters(): any{
    return CHARACTERS; 
      
  }
  getColumns(): string[]{
    return ["TaskId", "ParentTask", "TaskName", "Priority", "StartDateF","EndDateF"]};
  
  getTaskDetails(TaskId): any
  {
    CHARACTERS.filter(function test(x)
    {
      return x.Task===TaskId;

    }
  )
}
  selectedTask : Task;
  TaskList : Task[];
  task:string;
  
  getTaskData():Observable<any>
   {
    return this.http.get("http://localhost/TaskManagerWebAPI/GetAll");
  /*
     let obs = this.http.get("http://localhost:64915/GetAll");
     obs.subscrwbbibe((response)=>console.log(response));
     return obs;*/
   }
   getByTaskId(TaskId:any):Observable<any>
   {
    return this.http.get("http://localhost/TaskManagerWebAPI/GetById?TaskId=" + TaskId);

   }
   postTaskData(TaskDetail:any):Observable<any>
   {
    console.log(TaskDetail);  
    return this.http.post("http://localhost/TaskManagerWebAPI/PostTask/",
    TaskDetail) 
    .map(res => res );
    //.map((response:Response)=><any>response.json());
  
   }
   putTaskData(TaskDetail:any):Observable<any>
   {
    
    return this.http.put("http://localhost/TaskManagerWebAPI/UpdateTask/",
    TaskDetail) 
    .map(res => res );
    //.map((response:Response)=><any>response.json());
  
   }
   
   deleteTaskData(TaskDetail:any):Observable<any>
   {
    console.log('INSIDE SERVICE' + JSON.stringify(TaskDetail) );
    console.log(JSON.stringify(TaskDetail.TaskId));  
    return this.http.delete("http://localhost/TaskManagerWebAPI/DeleteTask?id=" + JSON.stringify(TaskDetail.TaskId)
    
    
    ) 
    .map(res => res );
    //.map((response:Response)=><any>response.json());
  
   }
  endTaskData(TaskDetail:any):Observable<any>
  {  
    
    return this.http.put("http://localhost/TaskManagerWebAPI/EndTask/" , TaskDetail)
    .map(res=>res);
   
  }
  }
  


